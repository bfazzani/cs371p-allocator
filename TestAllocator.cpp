// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#define TEST

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

/*
TEST(AllocatorFixture, test0) {
    using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
}
*/

using allocator_type = my_allocator<double, 1000>;
using value_type     = typename allocator_type::value_type;
using size_type      = typename allocator_type::size_type;
using pointer        = typename allocator_type::pointer;
using iterator       = typename allocator_type::iterator;
using const_iterator = typename allocator_type::const_iterator;


TEST(AllocatorFixture, test0) {
    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);

    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    } else {
        FAIL();
    }
}

TEST(AllocatorFixture, test1) {
    allocator_type x;                                            // read/write
    ASSERT_EQ(x[0], 992);
}

TEST(AllocatorFixture, test2) {
    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[0], 992);
}

TEST(AllocatorFixture, test3) {
    allocator_type x;
    allocator_type y;

    ASSERT_EQ(x == y, false);
}

TEST(AllocatorFixture, test4) {
    allocator_type x;
    iterator b = x.begin();
    iterator p = b;

    ASSERT_EQ(p == b, true);
}

TEST(AllocatorFixture, test5) {
    allocator_type x;
    iterator e = x.end();
    iterator p = e;

    ASSERT_EQ(p == e, true);
}

TEST(AllocatorFixture, test6) {
    allocator_type x;
    iterator b = x.begin();
    iterator p = b;

    ASSERT_EQ(++p == b, false);
}

TEST(AllocatorFixture, test7) {
    allocator_type x;
    iterator e = x.end();
    iterator p = e;

    ASSERT_EQ(--p == e, false);
}

TEST(AllocatorFixture, test8) {
    allocator_type x;
    iterator p = x.begin();

    ASSERT_EQ(*p, 992);
}

TEST(AllocatorFixture, test9) {
    const allocator_type x;
    const_iterator b = x.begin();
    const_iterator p = b;

    ASSERT_EQ(p == b, true);
}

TEST(AllocatorFixture, test10) {
    const allocator_type x;
    const_iterator e = x.end();
    const_iterator p = e;

    ASSERT_EQ(p == e, true);
}

TEST(AllocatorFixture, test11) {
    const allocator_type x;
    const_iterator b = x.begin();
    const_iterator p = b;

    ASSERT_EQ(++p == b, false);
}

TEST(AllocatorFixture, test12) {
    const allocator_type x;
    const_iterator e = x.end();
    const_iterator p = e;

    ASSERT_EQ(--p == e, false);
}

TEST(AllocatorFixture, test13) {
    const allocator_type x;
    const_iterator p = x.begin();

    ASSERT_EQ(*p, 992);
}

TEST(AllocatorFixture, test14) {
    allocator_type x;
    x.allocate(5);

    int sentinels[] = {-40, 944};
    int* s = sentinels;

    iterator p = x.begin();
    iterator e = x.end();

    while (p != e) {
        ASSERT_EQ(*p, *s);
        ++p;
        ++s;
    }
}

TEST(AllocatorFixture, test15) {
    allocator_type x;
    x.allocate(5);
    x.allocate(3);

    int sentinels[] = {-40, -24, 912};
    int* s = sentinels;

    iterator p = x.begin();
    iterator e = x.end();

    while (p != e) {
        ASSERT_EQ(*p, *s);
        ++p;
        ++s;
    }
}

TEST(AllocatorFixture, test16) {
    allocator_type x;
    pointer a = x.allocate(5);
    x.allocate(3);
    x.deallocate(a, 5);

    int sentinels[] = {40, -24, 912};
    int* s = sentinels;

    iterator p = x.begin();
    iterator e = x.end();

    while (p != e) {
        ASSERT_EQ(*p, *s);
        ++p;
        ++s;
    }
}

TEST(AllocatorFixture, test17) {
    allocator_type x;
    pointer a = x.allocate(5);
    pointer b = x.allocate(3);
    x.allocate(3);
    x.deallocate(a, 5);
    x.deallocate(b, 3);

    int sentinels[] = {72, -24, 880};
    int* s = sentinels;

    iterator p = x.begin();
    iterator e = x.end();

    while (p != e) {
        ASSERT_EQ(*p, *s);
        ++p;
        ++s;
    }
}

TEST(AllocatorFixture, test18) {
    allocator_type x;
    pointer a = x.allocate(5);
    pointer b = x.allocate(3);
    pointer c = x.allocate(3);
    x.deallocate(a, 5);
    x.deallocate(b, 3);
    x.deallocate(c, 3);

    int sentinels[] = {992};
    int* s = sentinels;

    iterator p = x.begin();
    iterator e = x.end();

    while (p != e) {
        ASSERT_EQ(*p, *s);
        ++p;
        ++s;
    }
}

TEST(AllocatorFixture, test19) {
    my_allocator<double, 100> x;
    x.allocate(3);
    x.allocate(2);
    x.allocate(4);

    int sentinels[] = {-24, -16, -36};
    int* s = sentinels;

    my_allocator<double,100>::iterator p = x.begin();
    my_allocator<double,100>::iterator e = x.end();

    while (p != e) {
        ASSERT_EQ(*p, *s);
        ++p;
        ++s;
    }
}

TEST(AllocatorFixture, test20) {
    allocator_type x;
    x.allocate(123);

    int sentinels[] = {-992};
    int* s = sentinels;

    iterator p = x.begin();
    iterator e = x.end();

    while (p != e) {
        ASSERT_EQ(*p, *s);
        ++p;
        ++s;
    }
}

TEST(AllocatorFixture, test21) {
    allocator_type x;
    x.allocate(5);
    x.allocate(3);
    x.allocate(3);

    ASSERT_EQ(x.valid(), true);
}

TEST(AllocatorFixture, test22) {
    allocator_type x;
    x.allocate(5);
    x.allocate(3);
    x.allocate(3);

    x[44] = 40;

    ASSERT_EQ(x.valid(), false);
}

TEST(AllocatorFixture, test23) {
    allocator_type x;
    x.allocate(5);
    x.allocate(3);
    x.allocate(3);

    x[48] *= -1;
    x[72] *= -1;

    ASSERT_EQ(x.valid(), false);
}