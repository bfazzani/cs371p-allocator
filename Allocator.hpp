// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <cstdlib>   // abs

#include <iostream>

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            char* tmp_p = reinterpret_cast<char*>(_p);
            int blocksize = abs(*_p);
            // next block will be at _p + 2 sentinels + the block
            tmp_p += 2*sizeof(int) + blocksize;
            _p = reinterpret_cast<int*>(tmp_p);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            char* tmp_p = reinterpret_cast<char*>(_p);
            // need to look at sentinel for previous block
            int blocksize = *(tmp_p - sizeof(int));
            // next block will be at _p - 2 sentinels - the block
            tmp_p -= 2*sizeof(int) + blocksize;
            _p = reinterpret_cast<int*>(tmp_p);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            const char* tmp_p = reinterpret_cast<const char*>(_p);
            int blocksize = abs(*_p);
            // next block will be at _p + 2 sentinels + the block
            tmp_p += 2*sizeof(int) + blocksize;
            _p = reinterpret_cast<const int*>(tmp_p);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            const char* tmp_p = reinterpret_cast<const char*>(_p);
            // need to look at sentinel for previous block
            int blocksize = *(tmp_p - sizeof(int));
            // next block will be at _p - 2 sentinels - the block
            tmp_p -= 2*sizeof(int) + blocksize;
            _p = reinterpret_cast<const int*>(tmp_p);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
#ifdef TEST
    FRIEND_TEST(AllocatorFixture, test21);
    FRIEND_TEST(AllocatorFixture, test22);
    FRIEND_TEST(AllocatorFixture, test23);
    FRIEND_TEST(AllocatorFixture, test24);
#endif
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * <your documentation>
     */
    bool valid () const {
        const_iterator it = begin();
        const_iterator e = end();

        bool prev_isfree = false;
        while (it != e) {
            int blocksize = abs(*it);
            // check end sentinel
            const char* tmp = reinterpret_cast<const char*>(&(*it));
            int end = *reinterpret_cast<const int*>(tmp + sizeof(int) + blocksize);
            if (*it != end) return false;

            // adjacent free blocks == bad
            if (*it > 0 && prev_isfree) return false;

            prev_isfree = *it > 0;
            ++it;
        }
        assert(it == e);

        return true;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        if (N < sizeof(T) + (2 * sizeof(int))) throw std::bad_alloc();
        (*this)[0] = N - 8;
        (*this)[N-4] = N - 8;
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {
        if (n == 0) throw std::bad_alloc();

        pointer ret = nullptr;
        int* p = &(*this)[0];
        int bytesize = n * sizeof(T);

        while (p != &(*this)[N]) {
            int blocksize = abs(*p);
            char* tmp = reinterpret_cast<char*>(p);
            if (*p >= bytesize) {
                int remaining_size = blocksize - (bytesize + 2*sizeof(int));
                if (remaining_size < (int)sizeof(T)) {       // no free block
                    *p *= -1;
                    tmp += blocksize + sizeof(int);
                    *reinterpret_cast<int*>(tmp) *= -1;
                } else {
                    *p = -bytesize;                                     // write to busy start
                    tmp += bytesize + sizeof(int);
                    *reinterpret_cast<int*>(tmp) = -bytesize;           // write to busy end
                    tmp += sizeof(int);
                    *reinterpret_cast<int*>(tmp) = remaining_size;      // write to free start
                    tmp += sizeof(int) + remaining_size;
                    *reinterpret_cast<int*>(tmp) = remaining_size;      // write to free end
                }
                ret = pointer(p + 1);
                break;
            }
            tmp += blocksize + 2*sizeof(int);
            p = reinterpret_cast<int*>(tmp);
        }

        if (ret == nullptr) throw std::bad_alloc();
        assert(valid());
        return ret;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     */
    void deallocate (pointer p, size_type n) {
        int* start = reinterpret_cast<int*>(p);
        --start;
        int blocksize = -*start;
        int* end = reinterpret_cast<int*>(p + blocksize/sizeof(T));
        if (*start != *end || blocksize < n)
            throw std::invalid_argument("invalid pointer to deallocate");

        if (start != &(*this)[0] && *(start - 1) > 0) {     // prev block is free
            int prev_size = *(start - 1);
            char* tmp = reinterpret_cast<char*>(start-1);
            tmp -= prev_size;
            start = reinterpret_cast<int*>(tmp);
            --start;
            blocksize += prev_size + 8;
        }
        if (end != &(*this)[N-4] && *(end + 1) > 0) {         // next block is free
            int next_size = *(end + 1);
            char* tmp = reinterpret_cast<char*>(end+1);
            tmp += next_size;
            end = reinterpret_cast<int*>(tmp);
            ++end;
            blocksize += next_size + 8;
        }
        *start = blocksize;
        *end = blocksize;

        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
