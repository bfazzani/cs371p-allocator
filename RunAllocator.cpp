// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // cin, cout
#include <vector>   // vector
#include <algorithm>// sort

#include "Allocator.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    typedef pair<double*, int> dpi;
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    the acceptance tests are hardwired to use my_allocator<double, 1000>
    */

    int t;
    cin >> t;
    assert(t <= 100);

    string s;
    getline(cin, s);
    getline(cin, s);
    while (t--) {
        my_allocator<double, 1000> x;
        vector<dpi> pointers;
        while (getline(cin, s) && s != "") {
            int i = stoi(s);
            if (i > 0) {
                pointers.push_back(dpi(x.allocate(i), i));
            } else {
                int index = -i - 1;
                sort(pointers.begin(), pointers.end());
                x.deallocate(pointers[index].first, pointers[index].second);
                pointers.erase(pointers.begin() + index);
            }
        }

        for (auto it = x.begin(); it != x.end(); ++it) {
            if (it != x.begin()) cout << " ";
            cout << *it;
        }
        cout << endl;
    }


    return 0;
}
